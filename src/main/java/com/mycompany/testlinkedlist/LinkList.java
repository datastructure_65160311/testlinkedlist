/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.testlinkedlist;

/**
 *
 * @author informatics
 */
class LinkList
{
    private Link first; // ref to first link on list

    public void LinkList() // constructor
    {
        first = null; // no items on list yet
    }

    public boolean isEmpty() // true if list is empty
    {
        return (first==null);
    }
    
    public void insertFirst(int id, double dd)
    { // make new link
        Link newLink = new Link(id, dd);
        newLink.next = first; // newLink --> old first
        first = newLink; // first --> newLink
    }
    
    public void insertLast(int id, double dd){
        Link newLink = new Link(id, dd);
        Link temp = first;
        while(true){
            if(temp==null){
                temp = newLink;
                first.next = temp;
                break;
            }else{
                temp = temp.next;
            }
        }

    }
    
    public Link deleteFirst() // delete first item
    { // (assumes list not empty)
        Link temp = first; // save reference to link
        first = first.next; // delete it: first-->old next
        return temp; // return deleted link
    }
    
    public void deleteLast(){
        
    }
    
    public void displayList()
    {
        System.out.println("List (first-->last): ");
        Link current = first; // start at beginning of list
        while(current != null) // until end of list,
        {
            current.displayLink(); // print data
            current = current.next; // move to next link
        }
    }
    



}
